Requirement

PHP Version 5.6.31
Mysql Database

Cara termudah adalah dengan menginstall xampp/lampp yang mensupport PHP Versi 5.6.31

Langkah yang perlu dilakukan

`1. git clone https://gitlab.com/richardoctoey/bbwapp.git bbw/`

`2. extract here untuk file: yii-1.1.19.5790cb.zip`

`3. pastikan struktur direktori : bbwapi/ dan yii-1.1.19.5790cb/ berada dalam folder bbw/`

`4. Export bbw/bbwapi/data/db.sql ke database`

`5. Ubah konfigurasi database pada bbw/bbwapi/config/main.php`



kemudian buka http://localhost/bbw/bbwapi