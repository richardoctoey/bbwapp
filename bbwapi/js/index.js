$(document).ready(function(){

    /*
        1. load operator
        2. select operator then
        3. load voucher
        4. Proses Transaksi
    */


    //1
    $.ajax({
        url: "/bbw/bbwapi/index.php/operator/api",
    }).done(function(data){
        if(data.status){
            $.each(data.obj, function(o,i){
                $('#operator_id').append($(new Option(i.nama, i.id)));
            });
        }else{
            alert(data.msg);
        }

    });

    //2 & 3
    $("#operator_id").change(function(){
        var opid = $(this).val();
        if($(this).value!=''){
            $.ajax({
                url: "/bbw/bbwapi/index.php/voucher/api",
                data: {operator_id: opid},
            }).done(function(data){
                if(data.status){
                    $("#voucher_id").find('option').remove();
                    $.each(data.obj, function(o,i){
                        $('#voucher_id').append($(new Option(i.voucher+' - '+i.harga, i.id)));
                    });
                }else{
                    alert(data.msg);
                }
            });
        }
    });

    //4
    $("#proses_transaksi").click(function(){
        phoneno = $("#phone_no").val();
        vocid = $("#voucher_id").val();

        $(this).attr("disabled","disabled");
        $.ajax({
            url: "/bbw/bbwapi/index.php/transactions/createApi",
            data: {phone_no: phoneno, voucher_id: vocid},
        }).done(function(data){
            if(data.status){
                alert(data.msg);
                window.location = '/bbw/bbwapi/index.php';
            }else{
                alert(data.msg);
            }
        });
    });
});