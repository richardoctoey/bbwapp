<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Untuk demo return api pada halaman login ini, saya buat agar tidak menredirect. Bisa saja sih dibuat untuk panggil dengan ajax, tapi biarkan saja. Hanya untuk demo.</p>

<p>Setelah selesai login. silahkan pergi ke halaman index.php</p>

<div class="form">
    <div class="error">

    </div>

    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username" id="username"/></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" id="password"/></td>
        </tr>
        <tr>
            <td>
                <button id="login">Login</button>
            </td>
        </tr>
    </table>
</div><!-- form -->

<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/js/login.js', CClientScript::POS_END
);
?>
