<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<table>
    <tr>
        <td>No Telepon</td>
        <td><input type="text" name="phone_no" id="phone_no"/></td>
    </tr>
    <tr>
        <td>Operator</td>
        <td>
            <select id="operator_id" name="operator_id">
                <option disabled selected>Pilih Operator</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Voucher</td>
        <td>
            <select id="voucher_id" name="voucher_id"></select>
        </td>
    </tr>
    <tr>
        <td>
            <button id="proses_transaksi">Proses Transaksi</button>
        </td>
    </tr>
</table>

<?php
Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl.'/js/index.js',CClientScript::POS_END
);
?>