<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	public $modelClassname = null;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'getApi', 'createApi', 'updateApi', 'deleteApi', 'api'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	public function actionApi(){
        header('Content-Type: application/json');
        $models = new $this->modelClassname;
        $models = $models->findApi($_GET);
        if(!empty($models)){
            echo CJSON::encode(["status" => 1, 'msg' => 'Found', 'obj' => $models]);
        }else {
            echo CJSON::encode(["status" => 0, 'msg' => 'Not Found', 'obj' => $models]);
        }
    }

	public function actionGetApi($id){
        header('Content-Type: application/json');
        $model = $this->loadModelApi($id);
	    echo CJSON::encode(["status"=>1,'msg'=>'Found','obj'=>$model]);
    }

    public function actionUpdateApi($id){
        header('Content-Type: application/json');
        $model = new $this->modelClassname;
	    $model = $this->loadModelApi($id);
	    $className = get_class($model);
	    $model->attributes = $_POST[$className];
	    if($model->save()){
	        echo CJSON::encode(['status'=>1, 'msg' => 'Success Update', 'id'=>$model->id]);
        }else{
            echo CJSON::encode(['status' => 0, 'msg' => $model->getErrorsApi(), 'id' => $model->id]);
        }
    }

    public function actionCreateApi()
    {
        header('Content-Type: application/json');
        $model = new $this->modelClassname;

        $className = get_class($model);
        $model->attributes = $_POST[$className];
        if ($model->save()) {
            echo CJSON::encode(['status' => 1, 'msg' => 'Success Create', 'id' => $model->id]);
        } else {
            echo CJSON::encode(['status' => 0, 'msg' => $model->getErrorsApi(), 'id' => $model->id]);
        }
    }

    public function actionDeleteApi($id)
    {
        header('Content-Type: application/json');
        $model = new $this->modelClassname;
        $model = $this->loadModelApi($id);
        if($model->delete()){
            echo CJSON::encode(['status' => 1, 'msg' => 'Success Delete', 'id' => $model->id]);
        } else {
            echo CJSON::encode(['status' => 0, 'msg' => $model->getErrorsApi(), 'id' => $model->id]);
        }
    }

    public function loadModelApi($id)
    {
        $model = new $this->modelClassname;
        $model = $model->findByPk($id);
        if ($model === null) {
            echo CJSON::encode(['status' => 0, 'msg' => 'Not Found']);
            Yii::app()->end();
        }
        return $model;
    }
}