<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 08/03/2018
 * Time: 21:28
 */

class ActiveRecord extends CActiveRecord
{
    public function findApi($gets)
    {
        return $this->findAllByAttributes($gets);
    }

    public function getErrorsApi(){
        $erros = parent::getErrors();
        $errs = '';
        foreach ($erros as $err) {
            foreach ($err as $i) {
                $errs .= ($i . "\n");
            }
        }
        return $errs;
    }
}